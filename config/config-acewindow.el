;; ace-window for navigating windows
(require-package 'ace-window)
(require 'ace-window)
(after 'ace-window
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  (custom-set-faces
   '(aw-leading-char-face
     ((t (:inherit ace-jump-face-foreground :height 4.0)))))
  )
(provide 'config-acewindow)
