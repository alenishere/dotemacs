(defun /editing/duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (newline)
  (yank)
  )

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(provide 'config-editing)
