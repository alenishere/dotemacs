(defcustom dotemacs-os/additional-exec-paths
  nil
  "Additional paths to be added to `exec-path'."
  :type '(repeat (string))
  :group 'dotemacs)

(setq exec-path-from-shell-check-startup-files nil)
(if (eq system-type 'windows-nt)
    (dolist (path (split-string (getenv "PATH") ";"))
      (add-to-list 'exec-path (replace-regexp-in-string "\\\\" "/" path)))
  (require-package 'exec-path-from-shell)
  (exec-path-from-shell-initialize))

(defun /os/addpath (path)
  (let* ((directory (expand-file-name path))
         (env-value (concat directory path-separator (getenv "PATH"))))
    (when directory
      (setenv "PATH" env-value)
      (setq eshell-path-env env-value)
      (add-to-list 'exec-path directory))))

(/os/addpath (concat user-emacs-directory "bin"))
(dolist (path dotemacs-os/additional-exec-paths)
  (/os/addpath path))

(when (eq system-type 'darwin)
  (require-package 'osx-trash)
  (osx-trash-setup)

  (require-package 'reveal-in-osx-finder)
  (require-package 'vkill))

(defun /os/reveal-in-os ()
  (interactive)
  (if (eq system-type 'windows-nt)
      (start-process "*explorer*" "*explorer*" "explorer.exe"
                     (replace-regexp-in-string "/" "\\\\" (file-name-directory (buffer-file-name))))
    (call-interactively #'reveal-in-osx-finder)))

;; Powershell windows-nt
(when (eq system-type 'windows-nt)

  )
;; To be used for windows-nt
;; (when (string-equal system-type "windows-nt")
;;   (let (
;;         (mypaths
;;          '(
;;            "C:/Python27"
;;            ;; "C:/Python32"
;;            "C:/strawberry/c/bin"
;;            "C:/strawberry/perl/site/bin"
;;            "C:/strawberry/perl/bin"

;;            "C:/Users/h3/AppData/Roaming/npm"
;;            "C:/Program Files (x86)/nodejs/"

;;            "C:/cygwin/usr/local/bin"
;;            "C:/cygwin/usr/bin"
;;            "C:/cygwin/bin"

;;            "C:/Program Files (x86)/ErgoEmacs/msys/bin"
;;            "C:/Program Files (x86)/Mozilla Firefox/"
;;            "C:/Program Files (x86)/Opera"
;;            "C:/Program Files (x86)/Safari"
;;            "C:/Users/h3/AppData/Local/Google/Chrome/Application"
;;            ) )
;;         )

;;     (setenv "PATH" (mapconcat 'identity mypaths ";") )

;;     (setq exec-path (append mypaths (list "." exec-directory)) )
;;     ) )
;; http://ergoemacs.org/emacs/emacs_env_var_paths.html

(provide 'config-os)
