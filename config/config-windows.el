(setq git-installtion-directory "c:/Program Files/Git")


(when (eq system-type 'windows-nt)
  ;; Slow emacs org mode when on windows
  (setq inhibit-compacting-font-caches t)
  ;; Grep and other GIT utilities
  (when (executable-find "git")     
    (setenv "PATH"
            (concat
             ;; Change this with your path to MSYS bin directory
             ;; "c:\\Program Files\\Git\\usr\\bin;"
             (concat (replace-regexp-in-string "/" "\\\\" git-installtion-directory) "\\usr\\bin;")
             (getenv "PATH")))
    (defun /utils/run-bash ()
      (interactive)
      (let ((shell-file-name (concat (replace-regexp-in-string "/" "\\\\" git-installtion-directory) "\\bin\\bash.exe"))
            (shell "*bash*")))
      ))
  ;; To modify the paths to emacs to Be readable
  ;; (add-to-list 'exec-path "C:/MinGW/bin")
  ;; (setenv "PATH" (mapconcat #'identity exec-path path-separator))  
  
  ;; Run  cmd.exe in emacs
  (defun /utils/run-cmdexe ()
    (interactive)
    (let ((shell-file-name "cmd.exe"))
      (shell "*cmd.exe*")))

  ;; Run powershell in emacs
  (defun /utils/run-powershell ()
    "Run powershell"
    (interactive)
    (async-shell-command "c:/windows/system32/WindowsPowerShell/v1.0/powershell.exe -Command -"
                         nil
                         nil))
  
  ;; Edit for R location
  (after 'org
    (setq org-babel-R-command "C:/Progra~1/R/R-2.15.0/bin/x64/R --slave --no-save"))

  ;; Xah find - a replacement of grep
  (/boot/delayed-init
   (require-package 'xah-find)
   (require 'xah-find))
  
  )

(provide 'config-windows)
