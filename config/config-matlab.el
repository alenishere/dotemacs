(/boot/lazy-major-mode "\\.m$" matlab-mode)
(after 'matlab-mode
  (setq matlab-indent-function t)
  (setq matlab-shell-command "matlab")
  (custom-set-variables
   '(matlab-shell-command-switches '("-nodesktop -nosplash")))
  (matlab-cedet-setup)
  (defun my-matlab-completion ()
    (add-to-list 'company-backends 'company-matlab-shell))
  (add-hook 'matlab-mode 'my-matlab-completion)
  )

(provide 'config-matlab)
