(after 'org
  (setq org-directory "~/Dropbox/Org")
  
  (defgroup dotemacs-org nil
    "Configuration options for org-mode."
    :group 'dotemacs
    :prefix 'dotemacs-org)

  (defcustom dotemacs-org/journal-file (concat org-directory "/journal.org")
    "The path to the file where you want to make journal entries."
    :type 'file
    :group 'dotemacs-org)

  (defcustom dotemacs-org/inbox-file (concat org-directory "/inbox.org")
    "The path to the file where to capture notes."
    :type 'file
    :group 'dotemacs-org)
  
  (defcustom dotemacs-org/gtd-file (concat org-directory "/gtd.org")
    "The path to the file where to capture notes."
    :type 'file
    :group 'dotemacs-org)
  
  (unless (file-exists-p org-directory)
    (make-directory org-directory))

  (setq org-default-notes-file (expand-file-name dotemacs-org/inbox-file))
  (setq org-log-done t)
  (setq org-log-into-drawer t)
  

  (setq org-startup-indented t)
  (setq org-indent-indentation-per-level 2)
  (setq org-src-fontify-natively t)
  
  (setq org-display-inline-images t)
  (setq org-redisplay-inline-images t)
  (setq org-startup-with-inline-images "inlineimages")
  
  (setq org-agenda-files `(,org-directory))
  (setq org-capture-templates
        `(("t" "Todo" entry (file+headline ,(expand-file-name dotemacs-org/inbox-file) "INBOX")
           "* TODO %?\n%U\n%a\n")
          ("n" "Note" entry (file+headline ,(expand-file-name dotemacs-org/inbox-file) "NOTES")
           "* %? :NOTE:\n%U\n%a\n")
          ("m" "Meeting" entry (file ,(expand-file-name dotemacs-org/inbox-file))
           "* MEETING %? :MEETING:\n%U")
          ("j" "Journal" entry (file+datetree ,(expand-file-name dotemacs-org/journal-file))
           "* %U\n** %?")
          ("s" "Shopping-out" entry (file+headline ,(expand-file-name dotemacs-org/gtd-file) "Shopping")
           "* TODO Buy %? :@OUTSIDE:\n%U")
          ("i" "Shopping-in" entry (file+headline ,(expand-file-name dotemacs-org/gtd-file) "Shopping")
           "* TODO Buy %? :@COMPUTER:\n%U")
          ))

  ;; (setq org-refile-targets
  ;;       '((nil :maxlevel . 3)
  ;;         (org-agenda-files :maxlevel . 2)))
  
  (setq org-use-fast-todo-selection t)
  (setq org-treat-S-cursor-todo-selection-as-state-change nil)
  (setq org-todo-keywords
        '((sequence "TODO(t)" "NEXT(n)" "UNDERPROG(u)" "DELEGATED(e@)" "|" "DONE(d)")
          (sequence "WAITING(w@/!)" "|" "CANCELLED(c@/!)")))

  (setq org-todo-state-tags-triggers
        ' (("CANCELLED" ("CANCELLED" . t))
           ("WAITING" ("WAITING" . t))
           ("TODO" ("WAITING") ("CANCELLED"))
           ("NEXT" ("WAITING") ("CANCELLED"))
           ("UNDERPROG" ("WAITING") ("CANCELLED"))
           ("DELEGATED" ("WAITING") ("CANCELLED"))
           ("DONE" ("WAITING") ("CANCELLED"))))


  (setq org-tag-persistent-alist 
        '((:startgroup . nil)
          ("@HOME" . ?h) 
          ("@OFFICE" . ?o)
          ("@PHONE" . ?p)
          ("@OUTSIDE" . ?t)
          ("@COMPUTER" . ?r)
          (:endgroup . nil)
          (:startgroup . nil)
          ("ONESTEP" . ?f) 
          ("LEARNING" . ?n)
          ("PROJECT" . ?j )
          ("PU" . ?q )
          (:endgroup . nil)
          (:startgroup . nil)
          ("enLO" . ?l)
          ("enMD" . ?m)
          ("enHI" . ?h)
          (:endgroup . nil)
          ("URGENT" . ?u)
          ("DO" . ?d)
          ("DECIDE" . ?e)
          ("DELEGATE" . ?g)
          ("DELETE" . ?b)  
          )
        )

  (setq org-tag-faces
        '(
          ("@HOME" . (:foreground "GoldenRod" :weight bold))
          ("@OFFICE" . (:foreground "GoldenRod" :weight bold))
          ("@PHONE" . (:foreground "GoldenRod" :weight bold))
          ("@OUTSIDE" . (:foreground "GoldenRod" :weight bold))
          ("@COMPUTER" . (:foreground "GoldenRod" :weight bold))   
          ("DEV" . (:foreground "IndianRed1" :weight bold))   
          ("FINANCE" . (:foreground "IndianRed1" :weight bold))
          ("LEARNING" . (:foreground "IndianRed1" :weight bold))
          ("PROJECT" . (:foreground "IndianRed1" :weight bold))
          ("PU" . (:foreground "IndianRed1" :weight bold))
          ("URGENT" . (:foreground "Red" :weight bold))  
          ("enHI" . (:foreground "Red" :weight bold))  
          ("enLO" . (:foreground "OrangeRed" :weight bold))  
          ("enMD" . (:foreground "OrangeRed" :weight bold))  
          ("DO" . (:foreground "OrangeRed" :weight bold))  
          ("DECIDE" . (:foreground "GoldenRod" :weight bold))
          ("DELEGATE" . (:foreground "LimeGreen" :weight ))
          ("DELETE" . (:foreground "LimeGreen" :weight ))
          ))
  
  (setq org-refile-targets '((nil :maxlevel . 9)
                             (org-agenda-files :maxlevel . 9)))
  (setq org-refile-use-outline-path 'file)
  (setq org-outline-path-complete-in-steps nil)
  (setq org-completion-use-ido t)

  (when (boundp 'org-plantuml-jar-path)
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((plantuml . t))))

  (add-hook 'org-babel-after-execute-hook #'org-redisplay-inline-images)

  (defun /org/org-mode-hook ()
    (toggle-truncate-lines t)
    (setq show-trailing-whitespace t))
  (add-hook 'org-mode-hook #'/org/org-mode-hook)

  (require-package 'ob-async)
  (require 'ob-async)

  (require-package 'org-bullets)
  (setq org-bullets-bullet-list '("●" "○" "◆" "◇" "▸"))
  (add-hook 'org-mode-hook #'org-bullets-mode)

  ;; Org contrib add-ons
  (require-package 'org-plus-contrib)
  (require 'org-checklist)
  (require 'org-habit)

  ;; Org aenda appearence
  (setq org-habit-graph-column 65)
  (setq org-agenda-tags-column 65)
  
  ;; Gnuplot initialization orgmode due to dependancy on table plotting
  (require-package 'gnuplot-mode)
  (unless (eq system-type 'windows-nt)
    (require-package 'gnuplot))
  (require 'gnuplot)
  
  ;; Shell in emacs
  (require 'ob-shell)
  ;; (when (eq system-type 'windows-nt)    
  ;;   (defadvice org-babel-sh-evaluate (around set-shell activate)
  ;;     "Add header argument :shcmd that determines the shell to be called."
  ;;     (let* ((org-babel-sh-command (or (cdr (assoc :shcmd params)) org-babel-sh-command)))
  ;;       ad-do-it
  ;;       ))
  ;;   )
  
  ;; Org babel Configuration - Need better organizing
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((gnuplot . t)
     (python . t)
     (R . t)
     ;;     (matlab .t)
     (emacs-lisp .t)     
     ;;     (shell .t)     
     )
   )


  ;; On demand load of all language in org-babel
  (defadvice org-babel-execute-src-block (around load-language nil activate)
    "Load language if needed"
    (let ((language (org-element-property :language (org-element-at-point))))
      (unless (cdr (assoc (intern language) org-babel-load-languages))
        (add-to-list 'org-babel-load-languages (cons (intern language) t))
        (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))
      ad-do-it))
  
  ;; Run code without confirmation
  (setq org-confirm-babel-evaluate nil)
  
  ;; R babel location
  (when (eq system-type 'windows-nt)
    (setq org-babel-R-command "C:/Progra~1/R/R-2.15.0/bin/x64/R --slave --no-save"))

  ;; Custom font for done items
  (setq org-fontify-done-headline t)
  (custom-set-faces
   '(org-done ((t (:foreground "Forest Green"
                               :weight normal
                               :strike-through t))))
   '(org-headline-done
     ((((class color) (min-colors 16) (background dark))
       (:foreground "LightSalmon" :strike-through t)))))
  
  ;; Todo keywoed face
  (setq org-todo-keyword-faces
        '(("TODO" . org-warning) ("UNDERPROG" . "violet") ("NEXT" . "dark cyan")
          ("CANCELED" . (:foreground "blue" :weight bold))))
  
  ;; Change todo state to done when sub items done. When statistical cookies present
  (defun /org/org-summary-todo (n-done n-not-done)
    "Switch entry to done when alll subentries are done, to TODO otherwise"
    (let (org-log-done org-log-states) ;; turn off logging
      (org-todo (if (= n-not-done 0) "DONE" "TODO"))))
  ;; Un-comment to activate hook on todo state change
  ;; (add-hook 'org-after-todo-statistics-hook #'/org/org-summary-todo)

  
  (defun /org/checkbox-list-complete ()
    (save-excursion
      (org-back-to-heading t)
      (let ((beg (point)) end)
        (end-of-line)
        (setq end (point))
        (goto-char beg)
        (if (re-search-forward "\\[\\([0-9]*%\\)\\]\\|\\[\\([0-9]*\\)/\\([0-9]*\\)\\]" end t)
            (if (match-end 1)
                (if (equal (match-string 1) "100%")
                    ;; all done - do the state change
                    (org-todo 'done)
                  (org-todo 'todo))
              (if (and (> (match-end 2) (match-beginning 2))
                       (equal (match-string 2) (match-string 3)))
                  (org-todo 'done)
                (org-todo 'todo)))))))
  (add-to-list 'org-checkbox-statistics-hook #'/org/checkbox-list-complete)
  
  ;; Timestamp  
  (add-hook 'org-mode-hook
            (lambda ()
              (add-hook 'before-save-hook 'time-stamp)
              ))

  ;; Clocking time
  (setq org-clock-persist 'history
        org-clock-persist-file (concat dotemacs-cache-directory "org-clock-save.el"))
  (org-clock-persistence-insinuate)

  ;; Update all statistical cookies
  (defun /org/update-all-statistical-cookies()
    (interactive)
    (let ((current-prefix-arg '(4)))
      (org-update-statistics-cookies "ALL")))

  ;; Sort Done to bottom and the order by priority
  (defun /org/todo-to-int (todo)
    (first (-non-nil
            (mapcar (lambda (keywords)
                      (let ((todo-seq
                             (-map (lambda (x) (first (split-string  x "(")))
                                   (rest keywords)))) 
                        (cl-position-if (lambda (x) (string= x todo)) todo-seq)))
                    org-todo-keywords))))

  (defun /org/org-sort-key ()
    (let* ((todo-max (apply #'max (mapcar #'length org-todo-keywords)))
           (todo (org-entry-get (point) "TODO"))
           (todo-int (if todo (/org/todo-to-int todo) todo-max))
           (priority (org-entry-get (point) "PRIORITY"))
           (priority-int (if priority (string-to-char priority) org-default-priority)))
      (format "%03d %03d" todo-int priority-int)
      ))

  (defun /org/org-sort-entries ()
    (interactive)
    (org-sort-entries nil ?f #'/org/org-sort-key))

  (defun /org/export-to-html()
    (interactive)
    (save-buffer)
    (setq current_buffer (current-buffer))
    (setq current_filename (buffer-file-name))
    (with-temp-buffer
      (shell-command (concat "code_to_org " current_filename) (current-buffer))
      (org-open-file (org-export-to-file 'html (concat current_filename ".html")))
      )
    )
  ;; Presentation in org-mode
  (require-package 'ox-reveal)
  (require 'ox-reveal)
  (setq org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js")
  (require-package 'htmlize)
  
  ;; Kanban in orgmode
  (require-package 'org-kanban)
  )

(after 'ob-plantuml
  (when (executable-find "npm")
    (let ((default-directory (concat user-emacs-directory "/extra/plantuml-server/")))
      (unless (file-exists-p "node_modules/")
        (shell-command "npm install"))

      (ignore-errors
        (let ((kill-buffer-query-functions nil))
          (kill-buffer "*plantuml-server*")))
      (start-process "*plantuml-server*" "*plantuml-server*" "npm" "start"))

    (defun init-org/generate-diagram (uml)
      (let ((url-request-method "POST")
            (url-request-extra-headers '(("Content-Type" . "text/plain")))
            (url-request-data uml))
        (let* ((buffer (url-retrieve-synchronously "http://localhost:8182/svg")))
          (with-current-buffer buffer
            (goto-char (point-min))
            (search-forward "\n\n")
            (buffer-substring (point) (point-max))))))

    (defun org-babel-execute:plantuml (body params)
      (let* ((out-file (or (cdr (assoc :file params))
                           (error "PlantUML requires a \":file\" header argument"))))
        (let ((png (init-org/generate-diagram (concat "@startuml\n" body "\n@enduml"))))
          (with-temp-buffer
            (insert png)
            (write-file out-file)))))))



(provide 'config-org)

