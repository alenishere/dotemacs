#+TITLE:Learning Python
#+AUTHOR: Alen Alex Ninan
#+email: alenalexninan@disroot.org
Time-stamp: <2020-02-16 20:15:48 alen>
#+STARTUP: showall
#+STARTUP: indent
#+STARTUP: align
#+STARTUP: inlineimages
#+ARCHIVE: %s_done::
#+OPTIONS: num:nil toc:nil


#+begin_src ipython :results raw drawer output :exports both :session learning
print("Start learning python")
#+end_src

* Reference Material
[[https://www.udemy.com/course/complete-python-bootcamp/learn/lecture/6350270?start=270#overview][Course Lecture: Python Bootcamp - Udemy]]
Ipyton Notebook link:
* Lesson 1: Numbers
** Integers.
Whole numbers, positive or negative.
** Floating point numbers.
Have decimal points in them, or use exponential (e) to define the number.

** Operation:
#+begin_src ipython :results raw drawer :exports both :session learning
2+1
#+end_src

#+RESULTS:
:results:
# Out [3]: 
# text/plain
: 3
:end:

#+begin_src ipython :results raw drawer :exports both :session learning
3-2
#+end_src

#+RESULTS:
:results:
# Out[6]:
: 1
:end:

#+begin_src ipython :results raw drawer :exports both :session learning
3*2
#+end_src

#+RESULTS:
:results:
# Out[8]:
: 6
:end:

#+begin_src ipython :results raw drawer :exports both :session learning
3/2
#+end_src

#+RESULTS:
:results:
# Out[9]:
: 1.5
:end:

In python 3 the division provides a result that is a float.

#+begin_src ipython :results raw drawer :exports both :session learning
# Exponents
4**5
#+end_src

#+RESULTS:
:results:
# Out[3]:
: 1024
:end:

- Order of operations in python: (PEDMAS)

** Variables:
#+begin_src ipython :results raw drawer output :exports both :session learning
a=10
print(a)
#+end_src

#+RESULTS:
:results:
10
:end:

1. Names cannot start with a number
2. There can be no spaces in the name, use _ instead
3. Can't use any of these symbols: '",<>?|\()!@#$%^&*~-+
4. It's considered best practice (PEP8) that the names are lower case

Using object names can be useful way to keep track of different variables in Python.
* Lesson 2: Strings
Used to record text information.
#+begin_src ipython :results raw drawer :exports both :session learning
'Hello'
#+end_src

#+RESULTS:
:results:
# Out [2]: 
# text/plain
: 'Hello'
:end:

#+begin_src ipython :results raw drawer :exports both :session learning
'This is also a string'
#+end_src

#+RESULTS:
:results:
# Out[3]:
: 'This is also a string'
:end:

#+begin_src ipython :results raw drawer :exports both :session learning
"I'm a string"
#+end_src

#+RESULTS:
:results:
# Out[4]:
: "I'm a string"
:end:

To display a string use "print()"
#+begin_src ipython :results raw drawer output :exports both :session learning
print("Hello world")
#+end_src

#+RESULTS:
:results:
# Out [3]: 
# output
Hello world

:end:

Print is a function in Python 3 while its not in python 2.

** String Basics.
#+begin_src ipython :results raw drawer output :exports both :session
s="Hello world"
#+end_src

#+RESULTS:
:results:
:end:


1. *Check length of a string*
   Use len() function
#+begin_src ipython :results raw drawer :exports both :session
len("Hello world")
#+end_src

#+RESULTS:
:results:
# Out[5]:
: 11
:end:

2. *Indexing strings*
Starts at 0
#+begin_src ipython :results raw drawer output :exports both :session
print(s[0])  # Extract first letter
print(s[10]) # Extract 11th letter
print(s[1:]) # Grab everything from Index 1 to end.
print(s[:3]) # grab everything upto.
print(s[-1]) # Can use negative for reverse index. Use -1 for final letter
print(s[:-1]) #Grab everything except last letter.
print(s[::2]) # Grab word letters with Interval of 2
print(s[::-1]) # GRab word letters in reverse.
#+end_src

#+RESULTS:
:results:
H
d
ello world
Hel
d
Hello worl
Hlowrd
dlrow olleH
:end:

1. *String properties*
   1. Strings are immutable - Cannot change the current included letters in the string. We Can concatenate them.
#+begin_src ipython :results raw drawer output :exports both :session
s = "Hello world"
s = s + ' concatenate me'
print (s)
letter='z'
letter=letter*10
print(letter)
#+end_src

#+RESULTS:
:results:
Hello world concatenate me
zzzzzzzzzz
:end:


   2. String methods.
   #+begin_src ipython :results raw drawer output :exports both :session
print(s.upper)
print(s.lower)
print(s.split('e')) # Return a list split by specified element

   #+end_src

   #+RESULTS:
   :results:
   <built-in method upper of str object at 0x7fbba4e4ff80>
   <built-in method lower of str object at 0x7fbba4e4ff80>
   ['H', 'llo world concat', 'nat', ' m', '']
   :end:

* Lesson 3: Print formats.
using .format method of string.
#+begin_src ipython :results raw drawer output :exports both :session
x='String'
print('Place My variable here: %s' %(x))
# %s converst the varibale into string
#+end_src

#+RESULTS:
:results:
Place My variable here: String
:end:

Printing floating point numbers
#+begin_src ipython :results raw drawer output :exports both :session
print('Floating Point number: %1.2f' %13.5478)
#+end_src

#+RESULTS:
:results:
Floating Point number: 13.55
:end:

* Lesson 4: Lists
Most general version of sequence in python.
Unlike strings lists are mutable.
** Creating Lists.
#+begin_src ipython :results raw drawer output :exports both :session
  my_lists=[1,2,3]
  print(my_lists)
  my_lists=['string', 23, 1.2, 'o']
  print(my_lists)
#+end_src

#+RESULTS:
:results:
[1, 2, 3]
['string', 23, 1.2, 'o']
:end:

** Indexing and slicing lists
#+begin_src ipython :results raw drawer output :exports both :session
my_lists=['one', 'two', 'three']
print(my_lists)
#+end_src

#+RESULTS:
:results:
['one', 'two', 'three']
:end:
#+begin_src ipython :results raw drawer output :exports both :session
print(my_lists[1:])
#+end_src

#+RESULTS:
:results:
['two', 'three']
:end:

#+begin_src ipython :results raw drawer output :exports both :session
my_lists=my_lists+ ["four"]
print(my_lists)
#+end_src

#+RESULTS:
:results:
['one', 'two', 'three', 'four']
:end:

Note:
1. Lists do not have size constrains
2. Do Not need to pre-define size.

** Basic list methods
1. Append method
#+begin_src ipython :results raw drawer output :exports both :session
l=[1, 2, 3]
print(l)
l.append('four')
print(l)
#+end_src

#+RESULTS:
:results:
[1, 2, 3]
[1, 2, 3, 'four']
:end:

2. Pop off item in a list
#+begin_src ipython :results raw drawer output :exports both :session
l.pop()
print(l)
x=l.pop(0)
print(x)
print(l)
#+end_src

#+RESULTS:
:results:
[1, 2, 3]
1
[2, 3]
:end:

3. Sorting lists
#+begin_src ipython :results raw drawer output :exports both :session
l2=['a', 'c', 'x', 'm']
print(l2)
l2.reverse()
print(l2)
l2.sort()
print(l2)
l2.sort(reverse=True)
print(l2)
#+end_src

#+RESULTS:
:results:
['a', 'c', 'x', 'm']
['m', 'x', 'c', 'a']
['a', 'c', 'm', 'x']
['x', 'm', 'c', 'a']
:end:

#+begin_src ipython :results raw drawer output :exports both :session
# A function that returns the length of the value:
def myFunc(e):
  return len(e)

cars = ['Ford', 'Mitsubishi', 'BMW', 'VW']

cars.sort(key=myFunc)
print(cars)
cars.sort(key=myFunc, reverse=True)
print(cars)
#+end_src

#+RESULTS:
:results:
['VW', 'BMW', 'Ford', 'Mitsubishi']
['Mitsubishi', 'Ford', 'BMW', 'VW']
:end:

** Nesting lists

#+begin_src ipython :results raw drawer output :exports both :session
l1=[1,2,3]
l2=[4,5,6]
l3=[7,8,9]
matrix=[l1, l2,l3]
print(l1)
print(l2)
print(l3)
print(matrix)
# to get element in the list inside a list
print(matrix[2][0])
#+end_src

#+RESULTS:
:results:
# Out [1]: 
# output
[1, 2, 3]
[4, 5, 6]
[7, 8, 9]
[[1, 2, 3], [4, 5, 6], [7, 8, 9]]
7

:end:

* Lesson 5: Dictionaries
Dictionary is a mapping. Uses key to access the element and is not determined by order and indexing.
#+begin_src ipython :results raw drawer :exports both :session
my_dict={'key1':'value', 'key2':'value2'}
my_dict['key1']
#+end_src

#+RESULTS:
:results:
# Out[5]:
: 'value'
:end:

Assigning key to dictionary
#+begin_src ipython :results raw drawer :exports both :session
d={}
d['animal']='Dog'
d
#+end_src

#+RESULTS:
:results:
# Out[6]:
: {'animal': 'Dog'}
:end:

Nesting of dictionaries.
#+begin_src ipython :results raw drawer output :exports both :session
d={'k1':{'k2':'value'}}
d['k1']['k2']
print(d['k1']['k2'].upper())
#+end_src

#+RESULTS:
:results:
VALUE
:end:

* Lesson 6: Files
Python uses file Objects to interact with external files in a computer.

Writing text file.
#+begin_src ipython :results raw drawer :exports both :session
pwd
#+end_src

#+RESULTS:
:results:
# Out[19]:
: '/home/alen/Dropbox/Projects/Python'
:end:

Create a text file at above location with name Test.txt to run further code.

1. Read a file:
#+begin_src ipython :results raw drawer :exports both :session
f=open('Test.txt')
f.read()
#+end_src

#+RESULTS:
:results:
# Out [3]: 
# text/plain
: 'noom1\nnoom2\nnoom3\nnoom4\nnoom5\n'
:end:

#+begin_src ipython :results raw drawer :exports both :session
f.read()
#+end_src

#+RESULTS:
:results:
# Out[21]:
: ''
:end:

The second time attempting to read returns nothing since the cursor now is at the end of te line to reset that we need to use seek method. 

#+begin_src ipython :results raw drawer :exports both :session
f.seek(0)
f.read()
#+end_src

#+RESULTS:
:results:
# Out [7]: 
# text/plain
: 'noom1\nnoom2\nnoom3\nnoom4\nnoom5\n'
:end:

Using readlines method
It returns all the lines in the files as list.

#+begin_src ipython :results raw drawer :exports both :session
f.seek(0)
f.readlines()
#+end_src

#+RESULTS:
:results:
# Out [6]: 
# text/plain
: ['noom1\n', 'noom2\n', 'noom3\n', 'noom4\n', 'noom5\n']
:end:

#+begin_src ipython :session
f=open('Test.txt')
for line in f:
      print(line)
#+end_src

#+RESULTS:
:results:
# Out [4]: 
# output
noom1

noom2

noom3

noom4

noom5


:end:

* Lesson 7: Sets and booleans

** Sets
#+begin_src ipython :results raw drawer output :exports both :session learning
x=set()
x.add(1)
x
#+end_src

#+RESULTS:
:results:
# Out [4]: 
# text/plain
: {1}
:end:

#+BEGIN_SRC ipython  :results raw drawer output :exports both :session learning
x.add(2)
x
#+END_SRC

#+RESULTS:
:results:
# Out [7]: 
# text/plain
: {1, 2}
:end:

#+BEGIN_SRC ipython  :results raw drawer output :exports both :session learning
x.add(1)
x
#+END_SRC

#+RESULTS:
:results:
# Out [8]: 
# text/plain
: {1, 2}
:end:
Sets can only Have Unique elements and are not impacted By order.

** Boolean
#+begin_src ipython :results raw drawer :exports both :session learning
# Conditional operator.
print(1>2)

# None Boolean
b=None
b # Nothing will be displayed.
#+end_src

#+RESULTS:
:results:
# Out [7]: 
# output
False

:end:

* Lesson 8: Tuples.
Tuples are similar to List but are immutable. Use  tuples to Present things That shouldn't be changed, such as days In a week, or dataes On a calendar.

#+begin_src ipython :results raw drawer :exports both :session learning
t=(1, 2, 3)
print(len(t))
t
#+END_SRC


#+RESULTS:
:results:
# Out [9]: 
# output
3

# text/plain
: (1, 2, 3)
:end:

#+BEGIN_SRC ipython  :results raw drawer :exports both :session learning
t.count(1)
t=(1, 1, 2, 3)
t.count(1)
#+end_src

#+RESULTS:
:results:
# Out [16]: 
# text/plain
: 2
:end:

Attempting to reassign tuple object
#+BEGIN_SRC ipython  :results raw drawer :exports both :session learning
t[0]='s'
#+end_src

Tuples are Immutable ad needs i Not to get changes then use tuple.

* Review:

* Lesson 9:
