;; Instructions:
;; -------------
;; Python assumed to be in path for python2 and python3 for windows. Anaconda not in path for windows.Anaconda in path for linux.
;; Make selection in select radio in "dotemacs-python/select".
;; Edit to point to the required folders in variables below.
;; Edit config for python for use of vertual environment in the toggle "dotemacs-python/venv".
;;Adding python to autoloads
(/boot/lazy-major-mode "\\.py$" python-mode)



(defgroup dotemacs-python nil
  "Configuration options for org-mode."
  :group 'dotemacs
  :prefix 'dotemacs-python)

;; Select python to use
(defcustom dotemacs-python/select
  'anaconda
  "Python programming mode config"
  :type '(radio
          (const :tag "python3" python3)
          (const :tag "python" python2)
          (const :tag "anaconda" anaconda)
          (const :tag "scimax" scimax)
          )
  :group 'dotemacs-python)

;; Toggle to enable vertual environment for python. 
(defcustom dotemacs-python/venv
  t
  "Integrates with venv. Venv used if non `nil'"
  :type 'boolean
  :group 'dotemacs-python)

;; Check if windows.
(defcustom dotemacs-python/system-check (eq system-type 'windows-nt)
  "When non-nil, disables integration with `vc.el'.
  This is non-nil by default on Windows machines"
  :type 'boolean
  :group 'dotemacs-python)

;; Edit to configure
;; When windows
(when dotemacs-python/system-check
  (setq default-venv-name "flowassurance") ;; edit name of default virtual environment
  (setq default-env-directory "c:/Anaconda/envs") ;; edit location for the default folder with virtual environment
  (setq anaconda-directory "c:/Anaconda") ;; edit the location for anaconda installation directory if selecetd.
  )
;; When other Operating system-type
(unless dotemacs-python/system-check
  (setq default-venv-name "flowassurance") ;; edit name of default virtual environment
  (setq default-env-directory "/home/alen/anaconda3/envs") ;; edit location for the default folder with virtual environment
  (setq anaconda-directory "/home/alen/anaconda3/") ;; edit the location for anaconda installation directory if selecetd.
  )


(require-package 'pip-requirements)

(setq default-venv-location (concat default-env-directory "/" default-venv-name "/"))

;; Settings for python 2
(when (eq dotemacs-python/select 'python)
  (add-hook 'python-mode-hook
            (lambda ()
              (setq indent-tabs-mode nil)
              (infer-indentation-style)
              (setq python-shell-interpreter "python")
              (after 'company
                ;; elpy installation
                (require-package 'pyvenv) 
                (require-package 'elpy)
                (elpy-enable)
                (after 'elpy
                  (setq elpy-rpc-python-command "python")
                  (when dotemacs-python/venv
                    (setenv "WORKON_HOME" default-env-directory)
                    (pyvenv-mode 1)
                    (pyvenv-tracking-mode 1)
                    (pyvenv-activate default-venv-location))
                  (add-to-list 'company-backends 'elpy-company-backend)                
                  ))))
  )

;; Settings for Python 3
(when (eq dotemacs-python/select 'python3)
  (add-hook 'python-mode-hook
            (lambda ()
              (setq indent-tabs-mode nil)
              (infer-indentation-style)
              (when dotemacs-python/system-check
                ;; something for windows if true
                ;; optional something if not
                (setq python-shell-interpreter "python"))
              (unless dotemacs-python/system-check
                (setq python-shell-interpreter "python3"))
              (after 'company
                ;; elpy installation
                (require-package 'pyvenv) 
                (require-package 'elpy)
                (elpy-enable)
                (after 'elpy
                  (when dotemacs-python/system-check
                    (setq elpy-rpc-python-command "python"))
                  (unless dotemacs-python/system-check
                    (setq elpy-rpc-python-command "python3"))
                  (when dotemacs-python/venv
                    (setenv "WORKON_HOME" default-env-directory)
                    (pyvenv-mode 1)
                    (pyvenv-tracking-mode 1)
                    (pyvenv-activate default-venv-location))
                  (add-to-list 'company-backends 'elpy-company-backend)                
                  ))))
  )

;; Setings for anaconda
(when (eq dotemacs-python/select 'anaconda)  
  
  ;; Adding anaconda to emacs path
  (when dotemacs-python/system-check
    (when (file-directory-p (concat anaconda-directory "/"))
      (setenv "PATH"
              (concat
               ;; Change this with your path to MSYS bin directory
               (concat (replace-regexp-in-string "/" "\\\\" anaconda-directory) ";" (replace-regexp-in-string "/" "\\\\" anaconda-directory) "\\Scripts;" (replace-regexp-in-string "/" "\\\\" anaconda-directory) "\\Library\\mingw-w64\\bin;" (replace-regexp-in-string "/" "\\\\" anaconda-directory) "\\Library\\bin;")
               (getenv "PATH")))))

  ;; (when dotemacs-python/system-check
  ;;   (when (file-directory-p (concat anaconda-directory "/"))
  ;;     (setenv "PATH"
  ;;             (concat
  ;;              ;; Change this with your path to MSYS bin directory
  ;;              (concat anaconda-directory ";" anaconda-directory "/Scripts;" anaconda-directory "/Library/mingw-w64/bin;" anaconda-directory "/Library/bin;")
  ;;              (getenv "PATH")))))

  (require-package 'anaconda-mode)
  (require-package 'pyvenv)  
  (when dotemacs-python/venv
    (setenv "WORKON_HOME" default-env-directory)
    (pyvenv-mode 1)
    (pyvenv-tracking-mode 1)
    (pyvenv-activate default-venv-location)
    )

  (add-hook 'python-mode-hook
            (lambda ()
              (setq indent-tabs-mode nil)
              (infer-indentation-style)
              (setq-default py-shell-name "ipython")
              (setq python-shell-interpreter "ipython"
                    python-shell-interpreter-args "-i --simple-prompt")              
              'anaconda-mode
              'anaconda-eldoc-mode                  
              ))

  (unless dotemacs-python/system-check
    (setq ob-ipython-command (concat anaconda-directory "bin/jupyter")))
  (require-package 'ob-ipython)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((ipython . t)))

  (after 'company
    (require-package 'company-anaconda)
    (add-to-list 'company-backends 'company-anaconda)
    ;; (add-to-list 'company-backends 'company-ob-ipython)
    )

  (after 'org
    ;; Completion ob-ipython
    (defun run-python-first (&rest args)
      "Start a inferior python if there isn't one."
      (or (comint-check-proc "*Python*") (run-python)))

    (advice-add 'org-babel-execute:ipython :after
                (lambda (body params)
                  "Send body to `inferior-python'."
                  (run-python-first)
                  (python-shell-send-string body)))

    (add-hook 'org-mode-hook
              (lambda ()
                (setq-local completion-at-point-functions
                            '(pcomplete-completions-at-point python-completion-at-point))))

    ;; Completion eldoc Configuration
    (defun ob-ipython-eldoc-function ()
      (when (org-babel-where-is-src-block-head)
        (python-eldoc-function)))

    (add-hook 'org-mode-hook
              (lambda ()
                (setq-default eldoc-documentation-function 'ob-ipython-eldoc-function)))

    ;; Ob-ipython help
    (defun ob-ipython-help (symbol)
      (interactive (list (read-string "Symbol: " (python-eldoc--get-symbol-at-point))))
      (unless (org-babel-where-is-src-block-head)
        (error "Symbol is not in src block."))
      (unless (ob-ipython--get-kernel-processes)
        (error "There is no ob-ipython-kernal running."))
      (when-let* ((processes  (ob-ipython--get-kernel-processes))
                  (session (caar processes))
                  (ret (ob-ipython--eval
                        (ob-ipython--execute-request (format "help(%s)" symbol) session))))
        (let ((result (cdr (assoc :result ret)))
              (output (cdr (assoc :output ret))))
          (let ((buf (get-buffer-create "*ob-ipython-doc*")))
            (with-current-buffer buf
              (let ((inhibit-read-only t))
                (erase-buffer)
                (insert output)
                (goto-char (point-min))
                (read-only-mode t)
                (pop-to-buffer buf)))))))
    ;; after org end
    )
  
  ;; Emacs ipython notebook.
  ;; (require-package 'ein)
  ;; (require 'ein)
  ;; ;; (require ob-ein)
  ;; (require 'ein-notebook)
  ;; (require 'ein-subpackages)
  
  ;; (org-babel-do-load-languages
  ;;  'org-babel-load-languages
  ;;  '((ein . t)))

  ;; emacs-jupyter for org
  ;; (require-package 'jupyter)
  ;; (require 'jupyter)
  ;; (require 'ob-jupyter)
  ;; (setq ob-async-no-async-languages-alist '("jupyter-python" "jupyter-julia"))
  ;; (org-babel-do-load-languages
  ;;  'org-babel-load-languages
  ;;  '((jupyter . t)))
  )


;; Setings for scimax
(when (eq dotemacs-python/select 'scimax)  
  
  ;; Adding anaconda to emacs path
  (when dotemacs-python/system-check
    (when (file-directory-p (concat anaconda-directory "/"))
      (setenv "PATH"
              (concat
               ;; Change this with your path to MSYS bin directory
               (concat (replace-regexp-in-string "/" "\\\\" anaconda-directory) ";" (replace-regexp-in-string "/" "\\\\" anaconda-directory) "\\Scripts;" (replace-regexp-in-string "/" "\\\\" anaconda-directory) "\\Library\\mingw-w64\\bin;" (replace-regexp-in-string "/" "\\\\" anaconda-directory) "\\Library\\bin;")
               (getenv "PATH")))))


  ;; Scimax
  (add-hook 'python-mode-hook
            (lambda ()
              (setq indent-tabs-mode nil)
              (infer-indentation-style)
              (setq-default py-shell-name "ipython")
              (setq python-shell-interpreter "ipython"
                    python-shell-interpreter-args "-i --simple-prompt")              
              anaconda-mode
              anaconda-eldoc-mode
              ))
  ;; (when dotemacs-python/venv
  ;;   (setenv "WORKON_HOME" default-env-directory)
  ;;   (pyvenv-mode 1)
  ;;   (pyvenv-tracking-mode 1)
  ;;   (pyvenv-activate default-venv-location))
  
  (after 'org
    (add-to-list 'load-path "~/.emacs.d/extra/scimax/ob-ipython-upstream/")
    (add-to-list 'load-path "~/.emacs.d/extra/scimax/")
    (require 'scimax-org-babel-python)
    (require 'ob-ipython)
    (require-package 'lispy)
    (lispy-mode 1)
    (require 'scimax-ob)
    (require 'scimax-org-src-blocks)
    (require 'scimax-org-babel-ipython-upstream)
    (require 'scimax-org-latex)
    (require 'scimax-org-eldoc)
    (require 'scimax-literate-programming)
    
    (setq ob-ipython-exception-results nil)
    (scimax-ob-ipython-turn-on-eldoc)
    (setq org-babel-async-ipython t)
    (add-to-list 'org-latex-minted-langs '(ipython "python"))
    )
  ;; Scimax end

  )
(provide 'config-python)
