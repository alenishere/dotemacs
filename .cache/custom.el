(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(ess ess-R-data-view ess-smart-equals ess-smart-underscore ess-view xah-find xah-fly-keys magit-imerge magit-lfs magit-libgit magit-org-todos ob-ammonite ob-applescript ob-axiom ob-blockdiag ob-browser ob-cfengine3 ob-clojurescript ob-coffeescript ob-crystal ob-cypher ob-dao ob-dart ob-diagrams ob-elixir ob-elm ob-elvish ob-fsharp ob-go ob-graphql ob-html-chrome ob-http ob-hy ob-ipython ob-kotlin ob-lfe ob-mermaid ob-ml-marklogic ob-mongo ob-nim ob-prolog ob-restclient ob-rust ob-sagemath ob-sml ob-sql-mode ob-svgbob ob-swift ob-tmux ob-translate ob-typescript ob-uart org-brain org-dashboard matlab-mode org-edit-latex org-ehtml org-elisp-help org-present org-sql org-radiobutton ox-clip ox-haunt ox-asciidoc ox-bibtex-chinese ox-epub ox-html5slide ox-hugo ox-impress-js ox-ioslide ox-jekyll-md ox-jira ox-json ox-latex-subfigure ox-mdx-deck ox-mediawiki ox-minutes ox-nikola ox-pandoc ox-pukiwiki ox-qmd ox-rfc ox-rst ox-slack ox-slimhtml ox-spectacle ox-textile ox-tiddly ox-trac ox-tufte ox-twbs ox-twiki ox-wk pandoc undo-tree ample-theme zenburn-theme yasnippet xterm-color windsize which-key wgrep web-mode vlf transpose-frame spaceline shackle restart-emacs rainbow-mode rainbow-delimiters pyvenv python-mode pip-requirements pcmpl-git pcache paradox page-break-lines ox-reveal origami org-plus-contrib org-kanban org-bullets ob-async neotree multiple-cursors material-theme markdown-toc markdown-preview-eww markdown-mode+ lua-mode lsp-mode lispy kaolin-themes jupyter ido-vertical-mode htmlize highlight-symbol highlight-quoted highlight-numbers helpful helm-swoop helm-projectile helm-flx helm-descbinds helm-dash helm-ag gruvbox-theme gnuplot-mode gnuplot git-timemachine git-gutter-fringe+ flycheck-pos-tip flx-ido fancy-narrow expand-region exec-path-from-shell eval-sexp-fu eshell-z elisp-slime-nav ein dumb-jump discover-my-major diminish diff-hl counsel-projectile company-quickhelp company-qml company-posframe company-php company-math company-fuzzy company-flx company-dict company-anaconda color-theme-sanityinc-tomorrow color-identifiers-mode apropospriate-theme all-the-icons aggressive-indent)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 4.0))))
 '(org-done ((t (:foreground "PaleGreen" :weight normal :strike-through t))))
 '(org-headline-done ((((class color) (min-colors 16) (background dark)) (:foreground "LightSalmon" :strike-through t)))))
