(after 'helm
  (define-key helm-map (kbd "TAB") #'helm-execute-persistent-action)
  (define-key helm-map (kbd "<tab>") #'helm-execute-persistent-action)
  (define-key helm-map (kbd "C-z") #'helm-select-action)
  (global-set-key (kbd "M-y") 'helm-show-kill-ring)
  
  ;; End of after 'helm
  )
