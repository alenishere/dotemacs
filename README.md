# dotemacs

This is my personal Emacs config.

# intro

There are many emacs configs, what makes this one different?

## kiss

This is a keep it simple stupid config.  It is built with 3 simple building blocks; small enough that it is white magic instead of black magic.

### simple building block 1

``` cl
(defun require-package (package)
  "Ensures that PACKAGE is installed."
  (unless (or (package-installed-p package)
              (require package nil 'noerror))
    (unless (assoc package package-archive-contents)
      (package-refresh-contents))
    (package-install package)))
```

The code here is self-explanatory.  This is how you declare what packages you want to install and use.  This was taken from [Purcell][1]'s config.

### simple building block 2

`with-eval-after-load` lets you defer execution of code until after a feature has been loaded.  This is used extensively throughout the config, so wrapping macro has been written for ease of use.  This is what keeps the config loading fast.

Another useful feature is that it can also be used to run code if a package has been installed by using `-autoloads`; e.g.

```cl
(after 'magit
  ;; execute after magit has been loaded
  )
(after "magit-autoloads"
  ;; execute if magit is installed/available
  )
(after [evil magit]
  ;; execute after evil and magit have been loaded
  )
```

This was taken from [milkypostman][2].

### simple building block 3

At the bottom of the `init.el` is the following gem:

``` cl
(cl-loop for file in (reverse (directory-files-recursively config-directory "\\.el$"))
  do (load file)))
```

Basically, it recursively finds anything in `config/` and loads it.  If you want to add additional configuration for a new language, simply create `new-language.el` in `config/` and it will automatically be loaded.  Files are loaded in reverse order so that any functions defined will be available in child nodes.

### other building blocks



#### bindings in one place

Another decision is to keep all keybindings in one place: `/bindings/**/*.el`.  Because of this, things like [use-package][3] aren't particularly useful here because it doesn't add much value over `(require-package)` and `after`.

Keybindings are the single most differentiating factor between configs.  By defining them in one place, if you want to use/fork this config, you can simply change the bindings to your liking and still use all the other preconfigured packages as is.  If you're not an Evil user, delete `config-evil.el` and you will get a pure Emacs experience.

#### lazy installation of major mode packages

By combining `after`, `require-package`, and `auto-mode-alist`, packages are installed only when necessary.  If you never open a Javascript file, none of those packages will be installed.

## Mode specific settings

### Themes 
Config for the themes are available in the folder 
`~/.emacs.d/core/themes.el`

### Org mode 
Edit org configuration file available in the folder `~/.emacs.d/config/config-org.el`.

1. Specify org agenda location with replacing "~/Dropbox/Org" in `(setq org-directory "~/Dropbox/Org")` with agenda file folder.
2. Add in org babel languages based on programming language frequently used. Search for `org-babel-do-load-languages` and add in the language to be supported.

### Python mode
Edit python configuration file available at the location `~/.emacs.d/config/comfig-python.el` to configure python. 
Replace `'anaconda` with python3, python to use python installed in system. To use scimax intergration in org mode use `'scimax`. Scimax autocomplete was Having issues working. So used the blog post in `'https://zwild.github.io/posts/ob-ipython-enhancement-completion-eldoc-help/` to make something work.
Edit the pyvenv-activate keyword in respective mode to edit the virtual environment to use.
<<<<<<< HEAD
Virtual environment have challenges working with scimax. Enable launch of anaconda base environment by default if jupyter installed using ananconda in linux to use scimax.
```
conda config --set auto_activate_base true
=======
Virtual environment have challenges working with scimax. Enable launch anaconda by default if jupyter installed using ananconda in linux.
```

Need to figure out how to get it working with windows.
### Perl mode

### C++ mode

### OLGA mode
OLGA genkey, Inp file synatx highlighting under works for emacs. Intergration to emacs org-mode also planned to be worked on. Will take time as I am busy with other activities.
Features to be added in:
1. Generic syntax highlighting
2. Company autocompletion
3. Genkey testing and review. (Should be easy if able to launch using eshell or shell)
4. Network component automated editing for easy file manipulation.
5. Auto import and analysis to orgmode for easy QA.

## MS Windows specific settings

1. Recommended to have git installed. Since grep not available by default. 
2. Configuration can be edited in `~/.emacs.d\config\config-windows.el` to configure emacs in windows.
3. Search and replace "c:\\Program Files\\Git" with the location to git installation folder in the file. Use dual backslash when providing location. 
4. Install the All-the-icons font available at the Link `https://github.com/domtronn/all-the-icons.el` or in the folder extras folder to display Icons in emacs.

## Mac specific settings

I don't have mac to test out or configure the settings. Not a huge fan of it. Work with windows and linux mint the most. If anyone could help with configuring for mac I will be thankful.

# install
```
git clone https://alenishere@bitbucket.org/alenishere/dotemacs.git
git submodule update --init --recursive

```
# disclaimer

here be dragons.

# license

MIT


[1]: https://github.com/purcell/emacs.d
[2]: http://milkbox.net/note/single-file-master-emacs-configuration/
[3]: https://github.com/jwiegley/use-package
[4]: https://github.com/bling/dotemacs
[5]: https://zwild.github.io/posts/ob-ipython-enhancement-completion-eldoc-help/
