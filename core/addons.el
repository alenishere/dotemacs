;;; All addons for emacs

;; Gnuplot for windows.
(when (eq system-type 'windows-nt)
  (add-to-list 'load-path "~/.emacs.d/extra/gnuplot-20141231.2137/")
  )

(provide 'addons)
