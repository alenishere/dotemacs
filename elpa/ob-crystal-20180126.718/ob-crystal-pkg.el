(define-package "ob-crystal" "20180126.718" "org-babel functions for Crystal evaluation"
  '((emacs "24.3"))
  :keywords
  '("crystal" "literate programming" "reproducible research")
  :authors
  '(("Brantou" . "brantou89@gmail.com"))
  :maintainer
  '("Brantou" . "brantou89@gmail.com")
  :url "https://github.com/brantou/ob-crystal")
;; Local Variables:
;; no-byte-compile: t
;; End:
