;;; ob-crystal-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ob-crystal" "ob-crystal.el" (0 0 0 0))
;;; Generated autoloads from ob-crystal.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-crystal" '("org-babel-")))

;;;***

;;;### (autoloads nil "test-ob-crystal" "test-ob-crystal.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from test-ob-crystal.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "test-ob-crystal" '("ob-crystal-test-" "org-")))

;;;***

;;;### (autoloads nil nil ("ob-crystal-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ob-crystal-autoloads.el ends here
