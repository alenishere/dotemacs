(define-package "ox-rfc" "20190926.851" "RFC Back-End for Org Export Engine"
  '((emacs "24.3")
    (org "8.3"))
  :keywords
  '("org" "rfc" "wp" "xml")
  :authors
  '(("Christian Hopps" . "chopps@devhopps.com"))
  :maintainer
  '("Christian Hopps" . "chopps@devhopps.com")
  :url "https://github.com/choppsv1/org-rfc-export")
;; Local Variables:
;; no-byte-compile: t
;; End:
