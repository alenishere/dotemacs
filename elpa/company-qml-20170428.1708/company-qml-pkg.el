(define-package "company-qml" "20170428.1708" "Company backend for QML files"
  '((qml-mode "0.1")
    (company "0.8.12"))
  :keywords
  '("extensions")
  :authors
  '(("Junpeng Qiu" . "qjpchmail@gmail.com"))
  :maintainer
  '("Junpeng Qiu" . "qjpchmail@gmail.com"))
;; Local Variables:
;; no-byte-compile: t
;; End:
