;;; ob-dart-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ob-dart" "ob-dart.el" (0 0 0 0))
;;; Generated autoloads from ob-dart.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-dart" '("org-babel-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ob-dart-autoloads.el ends here
