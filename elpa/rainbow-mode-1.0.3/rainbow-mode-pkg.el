;;; Generated package description from /home/alen/.emacs.d/elpa/rainbow-mode-1.0.3/rainbow-mode.el  -*- no-byte-compile: t -*-
(define-package "rainbow-mode" "1.0.3" "Colorize color names in buffers" 'nil :url "http://elpa.gnu.org/packages/rainbow-mode.html" :keywords '("faces") :authors '(("Julien Danjou" . "julien@danjou.info")) :maintainer '("Julien Danjou" . "julien@danjou.info"))
