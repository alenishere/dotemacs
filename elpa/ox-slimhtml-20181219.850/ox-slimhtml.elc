;ELC   
;;; Compiled
;;; in Emacs version 28.0.50
;;; with all optimizations.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\207" [require ox-html cl-lib] 2)
#@147 Transcode BOLD from Org to HTML.

CONTENTS is the text with bold markup.
INFO is a plist holding contextual information.

(fn BOLD CONTENTS INFO)
(defalias 'ox-slimhtml-bold #[771 "\205 \300\301\"\207" [format "<strong>%s</strong>"] 6 (#$ . 480)])
#@153 Transcode ITALIC from Org to HTML.

CONTENTS is the text with italic markup.
INFO is a plist holding contextual information.

(fn ITALIC CONTENTS INFO)
(defalias 'ox-slimhtml-italic #[771 "\205 \300\301\"\207" [format "<em>%s</em>"] 6 (#$ . 738)])
#@140 Transcode VERBATIM string from Org to HTML.

CONTENTS is nil.
INFO is a plist holding contextual information.

(fn VERBATIM CONTENTS INFO)
(defalias 'ox-slimhtml-verbatim #[771 "\300\301\211;\203 \302\303#\266\202\202 \304A@\"\266\202!\211\205# \305\306\"\207" [org-html-encode-plain-text :value get-text-property 0 plist-get format "<kbd>%s</kbd>"] 10 (#$ . 996)])
#@171 Transcode HEADLINE from Org to HTML.

CONTENTS is the section as defined under the HEADLINE.
INFO is a plist holding contextual information.

(fn HEADLINE CONTENTS INFO)
(defalias 'ox-slimhtml-headline #[771 "\300\301\211;\203 \302\303#\266\202\202 \304A@\"\266\202\"\305\"\306\211;\2031 \302\303#\266\202\2029 \304A@\"\266\202\307\211;\203K \302\303#\266\202\202S \304A@\"\266\202\211\205q \310\211;\203i \302\303#\266\202\202q \304A@\"\266\202\203\207 \311\312\313\314\315\316\315\317\n!DD\"!\"\262\205\242 \320\230?\205\242 \311\321\203\240 \311\322\"\202\241 \320#\323	\"\204\304 \311\324\206\265 \320		\206\277 \320&\202\344 \325	\"\205\316 \326\311\327\206\330 \320#\330\n\"\205\343 \331Q\205\374 \320\230?\205\374 \311\332\333\303\334\335\n\"#\"Q\207" [org-export-data :title get-text-property 0 plist-get org-export-get-relative-level :ATTR_HTML :HTML_CONTAINER :HTML_CONTAINER_CLASS format " %s" org-html--make-attribute-string org-export-read-attribute attr_html nil split-string "" "<%s%s>" " class=\"%s\"" org-export-low-level-p "<h%d%s>%s</h%d>%s" org-export-first-sibling-p "<ul>" "<li>%s%s</li>" org-export-last-sibling-p "</ul>" "</%s>" cl-subseq cl-search " "] 18 (#$ . 1377)])
#@262 Transcode a SECTION element from Org to HTML.

CONTENTS is the contents of the section.
INFO is a plist holding contextual information.

Sections are child elements of org headlines;
'container' settings are found in slim-headlines.

(fn SECTION CONTENTS INFO)
(defalias 'ox-slimhtml-section #[771 "\207" [] 4 (#$ . 2641)])
#@142 Transcode LINK from Org to HTML.

CONTENTS is the text of the link.
INFO is a plist holding contextual information.

(fn LINK CONTENTS INFO)
(defalias 'ox-slimhtml-link #[771 "\300\301\"\203 \302\211;\203 \303\304#\207\305A@\"\207\204< \306\307\310\211;\2032 \303\304#\266\202\202: \305A@\"\266\202\"\207\311\211;\203M \303\304#\266\202\202U \305A@\"\266\202\302\211;\203f \303\304#\266\202\202n \305A@\"\266\202\300\312\"\203\224 \313\314\211;\203\210 \303\304#\266\203\202\220 \305A@\"\266\203!\202\225 \315\316\317\230\203\305\320\"\206\244 \315\305\321\"\305\322\"\310\n\211;\203\300 \303\304#\266\202\202\310 \305A@\"\266\202\206\314 \315\306\203\334 \323!\203\334 \324\202\335 \315\203\n\325!\206\350 \315\227\326\230\203\n\203\315\230\204\327!\330Q\202\327!\202P$\266\204\202\306	$\207" [ox-slimhtml--immediate-child-of-p link :raw-link get-text-property 0 plist-get format "<em>%s</em>" :path :type paragraph ox-slimhtml--attr :parent "" "<a href=\"%s\"%s>%s</a>" "file" :html-extension :html-link-use-abs-url :html-link-org-as-html file-name-absolute-p "file:" file-name-extension "org" file-name-sans-extension "."] 17 (#$ . 2973)])
#@175 Transcode a PLAIN-LIST string from Org to HTML.

CONTENTS is the contents of the list element.
INFO is a plist holding contextual information.

(fn PLAIN-LIST CONTENTS INFO)
(defalias 'ox-slimhtml-plain-list #[771 "\205? \300\211;\203 \301\302#\266\202\202 \303A@\"\266\202\211\304\267\202/ \305\2020 \306\2020 \307\2020 \310\262\311\312\313!%\262\207" [:type get-text-property 0 plist-get #s(hash-table size 3 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (ordered 35 unordered 39 descriptive 43)) "ol" "ul" "dl" nil format "<%s%s>%s</%s>" ox-slimhtml--attr] 10 (#$ . 4201)])
#@171 Transcode a PARAGRAPH element from Org to HTML.

CONTENTS is the contents of the paragraph.
INFO is a plist holding contextual information.

(fn PARAGRAPH CONTENTS INFO)
(defalias 'ox-slimhtml-paragraph #[771 "\205' \300\301\"\204 \300\302\"\203 \207\303\304\"\203  \305\306\"\207\305\307\310!#\207" [ox-slimhtml--immediate-child-of-p item special-block ox-slimhtml--has-immediate-child-of-p link format "<p>%s</p>" "<p%s>%s</p>" ox-slimhtml--attr] 7 (#$ . 4819)])
#@155 Transcode an EXAMPLE-BLOCK element from Org to HTML.

CONTENTS is nil.  INFO is a plist holding contextual information.

(fn EXAMPLE-BLOCK CONTENTS INFO)
(defalias 'ox-slimhtml-example-block #[771 "\300\"\211\205* \301\302\303\211;\203 \304\305#\266\202\202$ \306A@\"\266\202\206( \307#\207" [org-html-format-code format "<pre><code class=\"%s\">%s</code></pre>" :language get-text-property 0 plist-get "example"] 12 (#$ . 5301)])
#@153 Transcode an EXPORT-BLOCK element from Org to HTML.

CONTENTS is nil.  INFO is a plist holding contextual information.

(fn EXPORT-BLOCK CONTENTS INFO)
(defalias 'ox-slimhtml-export-block #[771 "\300\211;\203 \301\302#\266\202\202 \303A@\"\266\202\304\211;\203* \301\302#\266\202\2022 \303A@\"\266\202\205S \211\305\230\203C \306\307\"\202S \211\310\230\203P \306\311\"\202S \312!\207" [:value get-text-property 0 plist-get :type "JAVASCRIPT" format "<script>%s</script>" "CSS" "<style type=\"text/css\">%s</style>" org-remove-indentation] 10 (#$ . 5749)])
#@155 Transcode a EXPORT-SNIPPET object from Org to HTML.

CONTENTS is nil.  INFO is a plist holding contextual information.

(fn EXPORT-SNIPPET CONTENTS INFO)
(defalias 'ox-slimhtml-export-snippet #[771 "\300\211;\203 \301\302#\266\202\202 \303A@\"\266\202\211\205 \211\207" [:value get-text-property 0 plist-get] 9 (#$ . 6329)])
#@187 Transcode SPECIAL-BLOCK from Org to HTML.

CONTENTS is the text within the #+BEGIN_ and #+END_ markers.
INFO is a plist holding contextual information.

(fn SPECIAL-BLOCK CONTENTS INFO)
(defalias 'ox-slimhtml-special-block #[771 "\205+ \300\211;\203 \301\302#\266\202\202 \303A@\"\266\202\227\304\305\306!%\262\207" [:type get-text-property 0 plist-get format "<%s%s>%s</%s>" ox-slimhtml--attr] 10 (#$ . 6669)])
#@175 Transcode SRC-BLOCK from Org to HTML.

CONTENTS is the text of a #+BEGIN_SRC...#+END_SRC block.
INFO is a plist holding contextual information.

(fn SRC-BLOCK CONTENTS INFO)
(defalias 'ox-slimhtml-src-block #[771 "\300\"\301\211;\203 \302\303#\266\202\202 \304A@\"\266\202\205* \305\306\307!$\207" [org-html-format-code :language get-text-property 0 plist-get format "<pre><code class=\"%s\"%s>%s</code></pre>" ox-slimhtml--attr] 10 (#$ . 7103)])
#@160 Return body of document string after HTML conversion.

CONTENTS is the transcoded contents string.
INFO is a plist holding export options.

(fn CONTENTS INFO)
(defalias 'ox-slimhtml-inner-template #[514 "\205S \300\230?\205S \301\302\"\211\205 \211\300\230?\205 \303\304\"\305\301\306\"\"\206) \300\305\301\307\"\"\2067 \300\205O \300\230?\205O \303\310\311\312\313\314\"#\"\260\262\207" ["" plist-get :html-container format "<%s>" ox-slimhtml--expand-macros :html-preamble :html-postamble "</%s>" cl-subseq 0 cl-search " "] 15 (#$ . 7571)])
#@157 Return full document string after HTML conversion.

CONTENTS is the transcoded contents string.
INFO is a plist holding export options.

(fn CONTENTS INFO)
(defalias 'ox-slimhtml-template #[514 "\301\302\303\"\"\302\304\"\305\302\306\"\"\305\302\307\"\"\302\310\"\302\311\"\302\312\"\302\313\"\314<\2036 @\262\205@ AP\315	\205K \316\n\317Q\320\321\322\230?\205] P\f\205\205 \f\322\230?\205\205 \203~ \323\324\325Q\326\327\"\"\202\205 \324\330R\322\230?\205\222 \nP\331\332\205\250 \322\230?\205\250 \333\334\"\320\205\300 \322\230?\205\300 \305\"\206\300 \322\305\302\335\"\"\206\317 \322\336\337\260\207" [org-html-doctype-alist assoc plist-get :html-doctype :language ox-slimhtml--expand-macros :html-head :html-head-extra :title :html-title :html-body-attr :html-header "\n" "<html" " lang=\"" "\"" ">" "<head>" "" format-spec "<title>" "</title>\n" format-spec-make 116 "</title>" "</head>" "<body" format " %s" :html-footer "</body>" "</html>"] 32 (#$ . 8141)])
#@159 Transcode a PLAIN-TEXT string from Org to HTML.

PLAIN-TEXT is the string to transcode.
INFO is a plist holding contextual information.

(fn PLAIN-TEXT INFO)
(defalias 'ox-slimhtml-plain-text #[514 "\300!\207" [org-html-encode-plain-text] 4 (#$ . 9190)])
#@188 Return ELEMENT's html attribute properties as a string.

When optional argument PROPERTY is non-nil, return the value of
that property within attributes.

(fn ELEMENT &optional PROPERTY)
(defalias 'ox-slimhtml--attr #[513 "\300\301#\211\203 \302\303!P\202 \304\207" [org-export-read-attribute :attr_html " " org-html--make-attribute-string ""] 6 (#$ . 9453)])
#@94 Is ELEMENT an immediate child of an org CONTAINER-TYPE element?

(fn ELEMENT CONTAINER-TYPE)
(defalias 'ox-slimhtml--immediate-child-of-p #[514 "\300\211;\203 \301\302#\266\203\202 \303A@\"\266\203\211\211:\204+ \211;\2053 \304\262\2025 \211@9\2053 \211@\262=\205m \305\211;\203K \301\302#\266\202\202S \303A@\"\266\202\306\211;\203d \301\302#\266\202\202l \303A@\"\266\202U\207" [:parent get-text-property 0 plist-get plain-text :begin :contents-begin] 10 (#$ . 9824)])
#@79 Does ELEMENT have an immediate ELEMENT-TYPE child?

(fn ELEMENT ELEMENT-TYPE)
(defalias 'ox-slimhtml--has-immediate-child-of-p #[514 "\300\301\302\303\304\305	!\306\"\307\310%\311\312%\207" [org-element-map make-byte-code 257 "\301\211;\203 \302\303#\266\202\202 \304A@\"\266\202\305\300\211;\203* \302\303#\266\202\2022 \304A@\"\266\202U\207" vconcat vector [:begin get-text-property 0 plist-get :contents-begin] 8 "\n\n(fn LINK)" nil t] 11 (#$ . 10322)])
#@167 Return CONTENTS string, with macros expanded.

CONTENTS is a string, optionally with {{{macro}}}
tokens.  INFO is a plist holding export options.

(fn CONTENTS INFO)
(defalias 'ox-slimhtml--expand-macros #[514 "\302\303\"\203` \304\305\306\"!\304\305\307\"!\305\310\"\206 \311\304\305\312\"!\313B\314\315\316\317\320\321	\320$\"B\322B\323BF\324\"	\325\326!r\211q\210\327\330\331\332\333!\334\"\335$\216	c\210\336!\210)\337 *\262\207\207" [org-macro-templates buffer-file-name cl-search "{{{" org-element-interpret-data plist-get :author :date :email "" :title "author" "date" format "(eval (format-time-string \"$1\" '%s))" org-read-date nil t "email" "title" org-combine-plists generate-new-buffer " *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 org-macro-replace-all buffer-string] 16 (#$ . 10800)])
#@233 Publish an org file to html.

PLIST is the property list for the given project.  FILENAME
is the filename of the Org file to be published.  PUB-DIR is
the publishing directory.

Return output file name.

(fn PLIST FILENAME PUB-DIR)
(defalias 'ox-slimhtml-publish-to-html #[771 "\301\302\"\206 \303\304\203 \305\230\204 \306P\202 \305%\207" [org-html-extension plist-get :html-extension org-publish-org-to slimhtml "" "."] 10 (#$ . 11681)])
(org-export-define-backend 'slimhtml '((bold . ox-slimhtml-bold) (example-block . ox-slimhtml-example-block) (export-block . ox-slimhtml-export-block) (export-snippet . ox-slimhtml-export-snippet) (headline . ox-slimhtml-headline) (inner-template . ox-slimhtml-inner-template) (italic . ox-slimhtml-italic) (item . org-html-item) (link . ox-slimhtml-link) (paragraph . ox-slimhtml-paragraph) (plain-list . ox-slimhtml-plain-list) (plain-text . ox-slimhtml-plain-text) (section . ox-slimhtml-section) (special-block . ox-slimhtml-special-block) (src-block . ox-slimhtml-src-block) (template . ox-slimhtml-template) (verbatim . ox-slimhtml-verbatim)) :menu-entry '(115 "Export to slimhtml" ((72 "As slimhtml buffer" ox-slimhtml-export-as-html) (104 "As slimhtml file" ox-slimhtml-export-to-html))) :options-alist '((:html-extension "HTML_EXTENSION" nil org-html-extension) (:html-link-org-as-html nil "html-link-org-files-as-html" org-html-link-org-files-as-html) (:html-doctype "HTML_DOCTYPE" nil org-html-doctype) (:html-container "HTML_CONTAINER" nil org-html-container-element t) (:html-link-use-abs-url nil "html-link-use-abs-url" org-html-link-use-abs-url) (:html-link-home "HTML_LINK_HOME" nil org-html-link-home) (:html-preamble "HTML_PREAMBLE" nil #1="" newline) (:html-postamble "HTML_POSTAMBLE" nil #1# newline) (:html-head "HTML_HEAD" nil org-html-head newline) (:html-head-extra "HTML_HEAD_EXTRA" nil org-html-head-extra newline) (:html-header "HTML_HEADER" nil #1# newline) (:html-footer "HTML_FOOTER" nil #1# newline) (:html-title "HTML_TITLE" nil "%t" t) (:html-body-attr "HTML_BODY_ATTR" nil #1# t)))
#@1089 Export current buffer to a SLIMHTML buffer.

Export as `org-html-export-as-html' does, with slimhtml
org-export-backend.

If narrowing is active in the current buffer, only export its
narrowed part.

If a region is active, export that region.

A non-nil optional argument ASYNC means the process should happen
asynchronously.  The resulting buffer should be accessible
through the `org-export-stack' interface.

When optional argument SUBTREEP is non-nil, export the sub-tree
at point, extracting information from the headline properties
first.

When optional argument VISIBLE-ONLY is non-nil, don't export
contents of hidden elements.

When optional argument BODY-ONLY is non-nil, only write code
between "<body>" and "</body>" tags.

EXT-PLIST, when provided, is a property list with external
parameters overriding Org default settings, but still inferior to
file-local settings.

Export is done in a buffer named "*Org SLIMHTML export*", which
will be displayed when `org-export-show-temporary-export-buffer'
is non-nil.

(fn &optional ASYNC SUBTREEP VISIBLE-ONLY BODY-ONLY EXT-PLIST)
(defalias 'ox-slimhtml-export-as-html #[1280 "\300\301\302\303&\207" [org-export-to-buffer slimhtml "*Org SLIMHTML Export*" #[0 "\300\301!\207" [set-auto-mode t] 2]] 14 (#$ . 13759) nil])
#@968 Export current buffer to an HTML file.

Export as `org-html-export-as-html' does, with slimhtml
org-export-backend.

If narrowing is active in the current buffer, only export its
narrowed part.

If a region is active, export that region.

A non-nil optional argument ASYNC means the process should happen
asynchronously.  The resulting file should be accessible through
the `org-export-stack' interface.

When optional argument SUBTREEP is non-nil, export the sub-tree
at point, extracting information from the headline properties
first.

When optional argument VISIBLE-ONLY is non-nil, don't export
contents of hidden elements.

When optional argument BODY-ONLY is non-nil, only write code
between "<body>" and "</body>" tags.

EXT-PLIST, when provided, is a property list with external
parameters overriding Org default settings, but still inferior to
file-local settings.

Return output file's name.

(fn &optional ASYNC SUBTREEP VISIBLE-ONLY BODY-ONLY EXT-PLIST)
(defalias 'ox-slimhtml-export-to-html #[1280 "\303\304\305\"\206 \206 \306P\307\"	\310\311					&)\207" [org-html-extension org-html-coding-system org-export-coding-system "." plist-get :html-extension "html" org-export-output-file-name org-export-to-file slimhtml] 15 (#$ . 15053) nil])
(provide 'ox-slimhtml)
