(define-package "axiom-environment" "20200109.2207" "An environment for using Axiom/OpenAxiom/FriCAS"
  '((emacs "24.2"))
  :keywords
  '("axiom" "openaxiom" "fricas")
  :authors
  '(("Paul Onions" . "paul.onions@acm.org"))
  :maintainer
  '("Paul Onions" . "paul.onions@acm.org"))
;; Local Variables:
;; no-byte-compile: t
;; End:
