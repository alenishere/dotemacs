;;; ob-elvish-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ob-elvish" "ob-elvish.el" (0 0 0 0))
;;; Generated autoloads from ob-elvish.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-elvish" '("ob-elvish-" "org-babel-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ob-elvish-autoloads.el ends here
