;;; ox-latex-subfigure-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ox-latex-subfigure" "ox-latex-subfigure.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ox-latex-subfigure.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-latex-subfigure" '("ox-latex-subfigure-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ox-latex-subfigure-autoloads.el ends here
