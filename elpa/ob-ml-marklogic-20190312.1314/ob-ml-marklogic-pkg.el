(define-package "ob-ml-marklogic" "20190312.1314" "org-babel functions for MarkLogic evaluation" 'nil :keywords
  '("marklogic" "xquery" "javascript" "sparql")
  :authors
  '(("Norman Walsh" . "ndw@nwalsh.com"))
  :maintainer
  '("Norman Walsh" . "ndw@nwalsh.com")
  :url "http://github.com/ndw/ob-ml-marklogic")
;; Local Variables:
;; no-byte-compile: t
;; End:
