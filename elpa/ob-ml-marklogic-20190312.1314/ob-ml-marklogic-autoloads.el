;;; ob-ml-marklogic-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ob-ml-common" "ob-ml-common.el" (0 0 0 0))
;;; Generated autoloads from ob-ml-common.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ml-common" '("ob-ml-common-")))

;;;***

;;;### (autoloads nil "ob-ml-javascript" "ob-ml-javascript.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from ob-ml-javascript.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ml-javascript" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-ml-sparql" "ob-ml-sparql.el" (0 0 0 0))
;;; Generated autoloads from ob-ml-sparql.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ml-sparql" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-ml-xquery" "ob-ml-xquery.el" (0 0 0 0))
;;; Generated autoloads from ob-ml-xquery.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ml-xquery" '("org-babel-")))

;;;***

;;;### (autoloads nil nil ("ob-ml-marklogic-pkg.el" "ob-ml-marklogic.el")
;;;;;;  (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ob-ml-marklogic-autoloads.el ends here
