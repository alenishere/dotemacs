;;; Generated package description from /home/alen/.emacs.d/elpa/restclient-20191009.1208/restclient.el  -*- no-byte-compile: t -*-
(define-package "restclient" "20191009.1208" "An interactive HTTP client for Emacs" 'nil :commit "e8ca809ace13549a1ddffb4e4aaa5d5fce750f3d" :keywords '("http") :authors '(("Pavel Kurnosov" . "pashky@gmail.com")) :maintainer '("Pavel Kurnosov" . "pashky@gmail.com"))
