(define-package "fsharp-mode" "20191130.1857" "Support for the F# programming language"
  '((emacs "25")
    (s "1.3.1")
    (dash "1.1.0")
    (eglot "0"))
  :keywords
  '("languages")
  :authors
  '(("1993-1997 Xavier Leroy, Jacques Garrigue and Ian T Zimmerman")
    ("2010-2011 Laurent Le Brun" . "laurent@le-brun.eu")
    ("2012-2014 Robin Neatherway" . "robin.neatherway@gmail.com")
    ("2017-2019 Jürgen Hötzel"))
  :maintainer
  '("Jürgen Hötzel"))
;; Local Variables:
;; no-byte-compile: t
;; End:
