;;; OLGA-mode.el --- sample major mode for editing OLGA. -*- coding: utf-8; lexical-binding: t; -*-

;; Copyright © 2025, by Alen Alex Ninan

;; Author: your name ( alenishere@gmail.com)
;; Version: 0.0.2
;; Created: 26 Jun 2019
;; Keywords: languages
;; Homepage: 

;; This file is not part of GNU Emacs.

;;; License:
;; You can redistribute this program and/or modify it under the terms of the GNU General Public License version 2.

;;; Commentary:

;; Syntax highlighting for OLGA genkey and input file for ease Of model review

;; full doc on how to use here

;;; Code:
(eval-when-compile
  (require 'cl-lib))

(require 'comint)
(require 'newcomment)
(require 'rx)

;; Local variables
(defgroup OLGA nil
  "Major mode for editing OLGA code."
  :prefix "OLGA-"
  :group 'languages)

(defcustom OLGA-indent-level 3
  "Amount by which OLGA subexpressions are indented."
  :type 'integer
  :group 'OLGA
  :safe #'integerp)

(defcustom OLGA-comment-start "!"
  "Default value of `comment-start'."
  :type 'string
  :group 'OLGA)

(defcustom OLGA-default-application "OLGA"
  "Default application to run in Lua process."
  :type '(choice (string)
                 (cons string integer))
  :group 'OLGA)

(defcustom OLGA-always-show t
  "*Non-nil means display OLGA-process-buffer after sending a command."
  :type 'boolean
  :group 'OLGA)

(defcustom OLGA-documentation-url
  (or (and (file-readable-p "/usr/share/doc/lua/manual.html")
           "file:///usr/share/doc/lua/manual.html")
      "http://www.lua.org/manual/5.1/manual.html")
  "URL pointing to the OLGA reference manual."
  :type 'string
  :group 'OLGA)

(defvar OLGA-process nil
  "The active OLGA process")

(defvar OLGA-process-buffer nil
  "Buffer used for communication with the OLGA process")


(defun OLGA--customize-set-prefix-key (prefix-key-sym prefix-key-val)
  (cl-assert (eq prefix-key-sym 'OLGA-prefix-key))
  (set prefix-key-sym (if (and prefix-key-val (> (length prefix-key-val) 0))
                          ;; read-kbd-macro returns a string or a vector
                          ;; in both cases (elt x 0) is ok
                          (elt (read-kbd-macro prefix-key-val) 0)))
  (if (fboundp 'OLGA-prefix-key-update-bindings)
      (OLGA-prefix-key-update-bindings)))

(defcustom OLGA-prefix-key "\C-c"
  "Prefix for all lua-mode commands."
  :type 'string
  :group 'OLGA
  :set 'OLGA--customize-set-prefix-key
  :get '(lambda (sym)
          (let ((val (eval sym))) (if val (single-key-description (eval sym)) ""))))

(defvar OLGA-mode-menu (make-sparse-keymap "OLGA")
  "Keymap for lua-mode's menu.")

(defvar OLGA-prefix-mode-map
  (eval-when-compile
    (let ((result-map (make-sparse-keymap)))
      (mapc (lambda (key_defn)
              (define-key result-map (read-kbd-macro (car key_defn)) (cdr key_defn)))
            '(("C-l" . OLGA-send-buffer)
              ("C-f" . OLGA-search-documentation)))
      result-map))
  "Keymap that is used to define keys accessible by `OLGA-prefix-key'.

If the latter is nil, the keymap translates into `OLGA-mode-map' verbatim.")


;; create the list for font-lock.
;; each category of keyword is given a particular face
(setq OLGA-font-lock-keywords
      (let* (
             ;; define several category of keywords
             (x-keywords '("CASE" "OPTIONS" "FILES" "DTCONTROL" "INTEGRATION" "TREND" "PROFILE" "RESTART" "WATEROPTIONS" "PROFILEDATA" "TRENDDATA" "OUTPUTDATA" "MATERIAL" "WALL" "CONNECTION" "SHUTIN" "HYDRATECURVE" "SOURCE" "WELL" "VALVE" "TRANSMITTER" "POSITION" "GEOMETRY" "PIPE" "HEATTRANSFER" "PARAMETERS" "BRANCH" "TUNING"))
             ;; (x-types '("float" "integer" "key" "list" "rotation" "string" "vector")) 
             (x-constants '("ACTIVE" "AGENT" "ALL_SIDES" "ATTACH_BACK"))
             (x-events '("at_rot_target" "at_target" "attach"))
             (x-functions '("NETWORKCOMPONENT" "ENDNETWORKCOMPONENT"))

             ;; generate regex string for each category of keywords
             (x-functions-regexp (regexp-opt x-functions 'words))
             (x-keywords-regexp (regexp-opt x-keywords 'words))
             ;; (x-types-regexp (regexp-opt x-types 'words)) 
             (x-constants-regexp (regexp-opt x-constants 'words))
             (x-events-regexp (regexp-opt x-events 'words))
             )

        `(
          (,x-functions-regexp . font-lock-function-name-face)
          ;; (,x-types-regexp . font-lock-type-face)
          (,x-constants-regexp . font-lock-constant-face)
          (,x-events-regexp . font-lock-builtin-face)
          (,x-keywords-regexp . font-lock-keyword-face)
          ;; note: order above matters, because once colored, that part won't change.
          ;; in general, put longer words first
          )))

;; Syntax table
(defvar OLGA-mode-syntax-table nil "Syntax table for `OLGA-mode'.")
(setq OLGA-mode-syntax-table
      (let ( (synTable (make-syntax-table)))
        ;; python style comment: “# …”
        (modify-syntax-entry ?! "<" synTable)
        (modify-syntax-entry ?\n ">" synTable)
        synTable))

;;;###autoload
(define-derived-mode OLGA-mode prog-mode "OLGA mode"
  "Major mode for editing OLGA (Oil and Gas Scripting Language)…"

  ;; code for syntax highlighting
  (setq font-lock-defaults '((OLGA-font-lock-keywords))))

;; add the mode to the `features' list
(provide 'OLGA-mode)

;;; OLGA-mode.el ends here
